#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import copy
import csv
import os
import sys
import numpy as np
import cv2
import time
import pytesseract

try:
    import Image
except ImportError:
    from PIL import Image

base_path = "text_extract/img/"

pattern_threshold = 0.2  # threshold which is used for determining color pattern, 0.09 < threshold < 0.3
bound_threshold = 5


def create_dir(dir_path):
    """
    Create Directory if not exists
    :param dir_path:
    :return:
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def sortkey_rect_y(a):
    """
    callback of sorting textlines via their y-axis
    :param a:
    :return:
    """
    return a[1]


def sortkey_rect_x(a):
    """
    callback of sorting textlines via their y-axis
    :param a:
    :return:
    """
    return a[0]


def paint_with_bg(src_img, axis):
    """
    Remove foreground in the part of image given by axis(x, y, w, h)
    Get most frequency color at the boundary of axis and fill entire area with that color
    :param src_img: source image
    :param axis: area which is referred to get background color
    :return: removed image
    """
    def get_most_frequency_color(dest_img, area):
        """
        Get most frequency color from the given image.
        :param dest_img:
        :param area:
        :return:
        """

        partial_img = dest_img[area[1]:area[1] + area[3], area[0]:area[0] + area[2]]

        max_point = [-1, -1, -1]
        # get max frequent color in the area
        index = 0
        for m in cv2.split(partial_img):
            unique, counts = np.unique(m, return_counts=True)
            max_index = list(counts).index(max(counts))
            max_val = unique[max_index]

            max_point[index] = int(max_val)
            index += 1

        return max_point

    [x1, y1, w1, h1] = axis

    # get most frequency color at the left boundary
    m_left = get_most_frequency_color(src_img, [x1-bound_threshold, y1, bound_threshold, h1])

    m_right = get_most_frequency_color(src_img, [x1+w1, y1, bound_threshold, h1])
    m_top = get_most_frequency_color(src_img, [x1, y1-bound_threshold, w1, bound_threshold])
    m_bottom = get_most_frequency_color(src_img, [x1, y1+h1, w1, bound_threshold])

    tmp_list = [m_left, m_right, m_top, m_bottom]
    m_list = copy.deepcopy(tmp_list)

    for m in tmp_list:
        # remove max color when no boundary
        if m[0] < 0 or m[1] < 0 or m[2] < 0:
            m_list.remove(m)

    # get color list out of 4 values
    color_r = [tmp[0] for tmp in m_list]
    color_g = [tmp[1] for tmp in m_list]
    color_b = [tmp[2] for tmp in m_list]

    m_total = []
    # get average value
    for col in [color_r, color_g, color_b]:
        m_total.append(reduce(lambda x, y: x + y, col) / len(col))

    # paint destination area with this most frequent color
    for j in range(3):
        src_img[y1:y1 + h1, x1:x1 + w1, j] = int(m_total[j])

    return src_img


def remove_foreground(file_path):
    """
    Remove foreground from image.
    :param file_path:
    :return:
    """

    img = cv2.imread(file_path)

    fgbg = cv2.createBackgroundSubtractorMOG2()

    fmask = fgbg.apply(img)
    cv2.imshow('frame', fmask)

    cv2.waitKey(0)


def sortkey(a):
    """
    callback of sort function
    :param a: item of list which is needed to sort
    :return: returns the final value - the sorting key is the final value
    """
    return a[-1]


def get_inner_freq(img_src, rr):
    """
    Get unique count in the area of image
    If this value is high, this area will be texture, otherwise will have simple color pattern
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    tmp_img = img_src[y:y + h, x:x + w]
    cnt = 0.0
    # get total count of different colors in each channel
    for m in cv2.split(tmp_img):
        unique, counts = np.unique(m, return_counts=True)
        max_cnt = max(counts)
        cnt += max_cnt / w / h

    # print("Color count: ", cnt / 3.0)
    return cnt / 3.0


def get_outer_freg(img_src, rr):
    """
    Get unique count in the boundary of area.
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    thr = bound_threshold
    # get square measure of boundary area
    hei = np.size(img_src, 0)
    wid = np.size(img_src, 1)
    if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
        cnt = 0.0
        # get total count of different colors in each channel
        # top side
        tmp_img = img_src[y - thr:y, x:x + w + thr]
        sq_meas = thr * (w + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # right side
        sq_meas = thr * (h + thr)
        tmp_img = img_src[y:y+h+thr, x+w:x+w+thr]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # bottom side
        sq_meas = thr * (w + thr)
        tmp_img = img_src[y+h:y+h+thr, x-thr:x+w]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # left side
        tmp_img = img_src[y - thr:y + h, x - thr:x]
        sq_meas = thr * (h + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas
        return cnt / 12.0

    else:
        print (rr, "  No boundary")
        return 0


# sorted by y, then X
def arrange_boxes(rr):
    return sorted(rr, key=lambda xx: (xx[1], xx[0]))


def extract_text(file_path, output_folder, mode):
    """
    Extract text lines from the given image and save metadata to csv file.
    :param file_path:  Image file full path
    :param output_folder: path of output files
    :param mode: extraction mode, 1: gradient morph, 0: normal
    :return:
    """
    if output_folder[-1] != "/":
        output_folder += "/"

    create_dir(output_folder)
    create_dir(output_folder + "TextLines")

    img_init = cv2.imread(file_path)    # initial image container
    img_final = cv2.imread(file_path)   # container which has rectangles of each text area
    img_buf = cv2.imread(file_path)     # container where each text area has been in-painted

    try:
        img2gray = cv2.cvtColor(img_init, cv2.COLOR_BGR2GRAY)
    except cv2.error:
        print("Error, cannot find file. Please check file name.")
        return -1

    height = np.size(img_init, 0)
    width = np.size(img_init, 1)

    if mode == 1:
        kernel = np.ones((3, 3), np.uint8)
        gradiented = cv2.morphologyEx(img2gray, cv2.MORPH_GRADIENT, kernel)

        ret, mask = cv2.threshold(gradiented, 180, 255, cv2.THRESH_BINARY)
        image_final = cv2.bitwise_and(gradiented, gradiented, mask=mask)
        ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
    else:
        ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
        image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
        ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text, cv.THRESH_BINARY_INV

    # to manipulate the orientation of dilution,
    # large x means horizontally dilating more, large y means vertically dilating more
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    dilated = cv2.dilate(new_img, kernel, iterations=6)  # dilate , more the iteration more the dilation

    contoured, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # get contours

    index = 1

    csv_path = output_folder + "metadata_text.csv"

    if contours is not None:
        rect_list = [cv2.boundingRect(contour) for contour in contours]
        rect_list.sort(key=sortkey_rect_x)
        rect_list.sort(key=sortkey_rect_y)

        remove_list = []
        with open(csv_path, 'wb') as csv_text:
            writer_text = csv.writer(csv_text, delimiter=',')
            for rect in rect_list:
                buf = ['TextLine' + str(index), ]
                # get rectangle bounding contour
                [x, y, w, h] = rect

                # Don't plot small false positives that aren't text
                if w < 35 and h < 35:
                    # print("Detected textline is too small, ", rect)
                    remove_list.append(rect)
                    continue
                # remove very large area
                val = 0
                if x < bound_threshold:
                    val += 1
                if x + w > width - bound_threshold:
                    val += 1
                if y < bound_threshold:
                    val += 1
                if y + h > height - bound_threshold:
                    val += 1
                if val >= 3:
                    remove_list.append(rect)
                    print("Detected textline is too large, ", rect)
                    continue

                # draw rectangle around contour on original image
                cv2.rectangle(img_final, (x, y), (x+w, y+h), (255, 0, 255), 2)

                # save cropped image and extract text
                cropped = img_init[y:y + h, x: x + w]
                s = output_folder + "TextLines/TextLine" + str(index) + "-1.png"
                cv2.imwrite(s, cropped)

                # extract text from the saved image file
                txt_img = Image.open(s)
                line_text = pytesseract.image_to_string(txt_img)

                if line_text.strip() == '':  # ignore un-detected text area
                    # os.remove(s)
                    remove_list.append(rect)
                    continue

                buf += [x, y, w, h, line_text]

                n = get_inner_freq(img_init, (x, y, w, h))
                if n < pattern_threshold:
                    buf.append('Box texture')
                else:
                    buf.append('Box plain')

                m = get_outer_freg(img_init, (x, y, w, h))

                if m == 0:
                    buf.append('Surround none')
                elif m < pattern_threshold:
                    buf.append('Surround texture')
                else:
                    buf.append('Surround plain')

                # append number of multi line
                line_number = get_line_number(line_text)
                buf.append(str(line_number))
                print("Text of number ", index, "Line count: ", line_number, "\n", line_text)
                writer_text.writerow(buf)

                print("LINE: ", line_text)

                paint_with_bg(img_buf, [x, y, w, h])

                index += 1

        for rm_item in remove_list:
            rect_list.remove(rm_item)

        if len(rect_list) > 0:
            # write original image with added contours to disk
            cv2.imshow(file_path.split('/')[-1] + ' - Text Detect', img_final)
            cv2.imshow('Inpainted Result', img_buf)
            result_folder = output_folder + "/all_objects/Object1"
            create_dir(result_folder)
            cv2.imwrite(result_folder + "/S1-1.png", img_buf)
            cv2.waitKey()

        return len(rect_list)

    else:
        print("No textline detected with mode ", mode)
        return 0


def get_line_number(line_str):
    """
    Return line number of multi-lined string
    :param line_str:
    :return:
    """
    blank_line = 0
    for line in line_str.splitlines():
        if line.strip() == '':
            blank_line += 1

    return len(line_str.splitlines()) - blank_line


def text_detect(img, ele_size=(8, 3)):
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_sobel = cv2.Sobel(img, cv2.CV_8U, 1, 0)  # same as default,None,3,1,0,cv2.BORDER_DEFAULT)
    img_threshold = cv2.threshold(img_sobel, 0, 255, cv2.THRESH_OTSU+cv2.THRESH_BINARY)
    element = cv2.getStructuringElement(cv2.MORPH_RECT, ele_size)
    img_threshold = cv2.morphologyEx(img_threshold[1], cv2.MORPH_CLOSE, element)
    contours = cv2.findContours(img_threshold, 0, 1)
    Rect = [cv2.boundingRect(i) for i in contours[1] if i.shape[0] > 100]
    RectP = [(int(i[0]-i[2]*0.08), int(i[1]-i[3]*0.08), int(i[0]+i[2]*1.1), int(i[1]+i[3]*1.1)) for i in Rect]

    print("Result: ")

    print(Rect)
    print(RectP)

    for r in Rect:
        cv2.rectangle(img, (r[0], r[1]), (r[2], r[3]), (255, 0, 0), 2)

    cv2.imshow("Result", img)
    cv2.waitKey()
    return RectP

if __name__ == '__main__':

    extentions = ['jpg', 'png']
    file_names = [fn for fn in os.listdir(base_path) if any(fn.endswith(ext) for ext in extentions)]

    file_names = ['1.png', ]
    # file_names = ['bmw.jpg', ]
    # file_names = ['best-buy_removed_box.jpg', 'bmw_removed_box.jpg']
    for fn in file_names:
        print("\n------Starting ", fn)
        s_time = time.time()
        f_name = base_path + "/" + fn

        output_dir = base_path + "/output/" + fn.split(".")[0]

        # If no text lines are detected with normal mode, try again with gradient mode.
        first_result = extract_text(f_name, str(output_dir), 0)
        print("First result: ", first_result)

        if first_result == 0:
            print("Retrying with gradient mode.")
            extract_text(f_name, str(output_dir), 1)

        # img = cv2.imread(f_name)
        # text_detect(img)

        print("Elapsed: ", time.time() - s_time)
        # remove_foreground(f_name)

    cv2.destroyAllWindows()
