import os

import cv2
import numpy as np


def captch_ex(file_name):
    img = cv2.imread(file_name)

    img_final = cv2.imread(file_name)
    img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    kernel = np.ones((3, 3), np.uint8)
    gradiented = cv2.morphologyEx(img2gray, cv2.MORPH_GRADIENT, kernel)

    ret, mask = cv2.threshold(gradiented, 0, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(gradiented, gradiented, mask=mask)
    ret, new_img = cv2.threshold(gradiented, 0, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
    '''
            line  8 to 12  : Remove noisy portion
    '''
    # to manipulate the orientation of dilution,
    # large x means horizonatally dilating  more, large y means vertically dilating more
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    dilated = cv2.dilate(new_img, kernel, iterations=6)  # dilate , more the iteration more the dilation

    contoured, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # get contours
    index = 0
    for contour in contours:
        # get rectangle bounding contour
        [x, y, w, h] = cv2.boundingRect(contour)

        # Don't plot small false positives that aren't text
        if w < 35 and h < 35:
            continue

        # draw rectangle around contour on original image
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 255), 2)

        '''
        #you can crop image and send to OCR  , false detected will return no text :)
        cropped = img_final[y :y +  h , x : x + w]

        s = file_name + '/crop_' + str(index) + '.jpg'
        cv2.imwrite(s , cropped)
        index = index + 1
        '''
    # write original image with added contours to disk
    cv2.imshow('captcha_result', img)
    cv2.waitKey()

if __name__ == '__main__':

    base_path = 'img'

    extentions = ['jpg', 'png']
    file_names = [fn for fn in os.listdir(base_path) if any(fn.endswith(ext) for ext in extentions)]

    file_names = ['test.png', ]
    for fn in file_names:
        captch_ex(base_path + "/" + fn)
