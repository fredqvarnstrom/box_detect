from PIL import Image
from tesserocr import PyTessBaseAPI, RIL
import argparse
import os
import csv
import wester_feature_extractor as fe
import cv2
import numpy as np

#image = Image.open('/Users/keithahern/Downloads/S1-1.png')
#:wqaimage = Image.open('/Users/keithahern/Downloads/Slide 2/1.png')

# test for textures
pattern_threshold = 0.2  # threshold which is used for determining color pattern, 0.09 < threshold < 0.3
bound_threshold = 5

def extract_text(input_file_name, output_folder_name):
        pil_image = Image.open(input_file_name)
        image = cv2.imread(input_file_name)
        print "Image " + str(pil_image.size)
        with open(output_folder_name + "/metadata_text.csv",'wb') as wcsvfile:
            writer = csv.writer(wcsvfile, delimiter=',')
            with PyTessBaseAPI() as api:
              api.SetImage(pil_image)
              boxes = api.GetComponentImages(RIL.TEXTLINE, True)
              print 'Found {} textline image components.'.format(len(boxes))
              ti = 1
              valid_textlines = []
              for i, (im, box, _, _) in enumerate(boxes):
                  # im is a PIL image object
                  # box is a dict with x, y, w and h keys
                  api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
                  ocrResult = api.GetUTF8Text()
                  conf = api.MeanTextConf()
                  print (u"Box[{0}]: x={x}, y={y}, w={w}, h={h}, "
                         "confidence: {1}, text: {2}").format(i, conf, ocrResult.encode('ascii', 'ignore'), **box)

                  if conf > 60 and box['h'] > 12 and (not ocrResult.isspace()):
                    text_folder_name = output_folder_name + "/TextLines/"
                    if not os.path.exists(os.path.dirname(text_folder_name)):
                      os.makedirs(os.path.dirname(text_folder_name))
                    bbox =  (box['x'], box['y'], box['x'] + box['w'], box['y'] + box['h'])
                    text_image = pil_image.crop(bbox)
                    text_image.save(text_folder_name + "/TextLine" + str(ti) + "-1.png")
                    row = ["TextLine" + str(ti), box['x'], box['y'], box['w'], box['h'], conf, ocrResult.encode('ascii', 'ignore') ]

                    n = get_inner_freq(image, [box['x'], box['y'], box['w'], box['h']])
                    if n < pattern_threshold:
                        row.append('Box texture')
                    else:
                        row.append('Box plain')

                    m = get_outer_freg(image, [box['x'], box['y'], box['w'], box['h']])

                    if m == 0:
                        row.append('Surround none')
                    elif m < pattern_threshold:
                        row.append('Surround texture')
                    else:
                        row.append('Surround plain')

                    writer.writerow(row)

                    valid_textlines.append([box['x'], box['y'], box['w'], box['h']])

                    ti = ti + 1

              # inpaint the text boxes
              if len(valid_textlines) > 0:
                new_image = remove_boxes(image, valid_textlines)
                result_name = output_folder_name + "/all_objects/Object1/"
                if not os.path.exists(os.path.dirname(result_name)):
                  os.makedirs(os.path.dirname(result_name))
                cv2.imwrite(result_name + "/S1-1.png", new_image)

def paint_with_bg(src_img, axis, dest_axis, number):
    """
    Remove foreground in the part of image given by axis(x, y, w, h)
    :param src_img: source image
    :param axis: area which is referred to get background color
    :param dest_axis: area which is needed to paint.
    :param number: referred number which is used in deciding background, see code below
    :return: removed image
    """
    img = src_img[axis[1]:axis[1] + axis[3], axis[0]:axis[0] + axis[2]]

    max_point = []
    # get max frequent color in the area
    for m in cv2.split(img):
        unique, counts = np.unique(m, return_counts=True)
        max_index = list(counts).index(max(counts))
        max_val = unique[max_index]

        if number == 2 and unique.size > 1:  # get the secondly frequent value
            # pop maximum value
            new_counts = np.delete(counts, max_index)
            new_unique = np.delete(unique, max_index)
            max_index = list(new_counts).index((max(new_counts)))
            max_val = new_unique[max_index]
        max_point.append(max_val)

    # paint destination area with ths most frequent color
    for j in range(3):
        src_img[dest_axis[1]:dest_axis[1] + dest_axis[3], dest_axis[0]:dest_axis[0] + dest_axis[2], j] = max_point[j]

    return src_img

def sortkey(a):
    """
    callback of sort function
    :param a: item of list which is needed to sort
    :return: returns the final value - the sorting key is the final value
    """
    return a[-1]

def remove_boxes(src_img, re_list):
    """
    Remove found boxes and save to file
    NOTE:  We need to sort rectangles to be sorted with their area.
           And then, while removing characters from the small rectangle, all characters in the boxes are removed
           After that, decide whether to remove box itself and remove it.

    :param src_img: source image
    :param re_list: list of 4-axis rectangles which was found before
    :return:
    """
    # append area to each rectangle for sorting
    for r in re_list:
        r.append(r[2] * r[3])
    # sort rectangles with their area size
    re_list.sort(key=sortkey)

    # at first, remove characters in the found boxes
    for r in re_list:
        r.pop(-1)  # pop area
        src_img = paint_with_bg(src_img, r, r, 1)

    # cv2.imshow('Removed foreground', src_img)

    # if every edge of box is not near to the edge of whole image, remove box itself
    thr = bound_threshold  # boundary of box : get color of box's boundary and fill box with this color
    for r in re_list:
        hei = np.size(src_img, 0)
        wid = np.size(src_img, 1)
        x, y, w, h = r
        if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
            bg_area = [x - thr, y - thr, w + 2 * thr, h + 2 * thr]
            dest_area = [x - 1, y - 1, w + 2, h + 2]
            src_img = paint_with_bg(src_img, bg_area, dest_area, 2)

    # cv2.imshow('Without Boxes', src_img)

    return src_img


def get_inner_freq(img_src, rr):
    """
    Get unique count in the area of image
    If this value is high, this area will be texture, otherwise will have simple color pattern
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    tmp_img = img_src[y:y + h, x:x + w]
    cnt = 0.0
    # get total count of different colors in each channel
    for m in cv2.split(tmp_img):
        unique, counts = np.unique(m, return_counts=True)
        max_cnt = max(counts)
        cnt += max_cnt / w / h

    print "Color count: ", cnt / 3.0
    return cnt / 3.0


def get_outer_freg(img_src, rr):
    """
    Get unique count in the boundary of area.
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    thr = bound_threshold
    # get square measure of boundary area
    hei = np.size(img_src, 0)
    wid = np.size(img_src, 1)
    if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
        cnt = 0.0
        # get total count of different colors in each channel
        # top side
        tmp_img = img_src[y - thr:y, x:x + w + thr]
        sq_meas = thr * (w + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # right side
        sq_meas = thr * (h + thr)
        tmp_img = img_src[y:y+h+thr, x+w:x+w+thr]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # bottom side
        sq_meas = thr * (w + thr)
        tmp_img = img_src[y+h:y+h+thr, x-thr:x+w]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # left side
        tmp_img = img_src[y - thr:y + h, x - thr:x]
        sq_meas = thr * (h + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas
        return cnt / 12.0

    else:
        print rr, "  No boundary"
        return 0


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file_name", required=True, help="Input Image File Name")
    parser.add_argument("-o", "--output_folder_name", required=True, help="Output Folder Name")

    inst_arg = parser.parse_args()
    args = vars(inst_arg)

    input_file_name = args['input_file_name']
    output_folder_name = args['output_folder_name']

    extract_text(input_file_name, output_folder_name)
