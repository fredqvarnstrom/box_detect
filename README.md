## Install **openCV**
    
    sudo apt-get autoremove libopencv-dev python-opencv
    
    wget https://raw.githubusercontent.com/milq/scripts-ubuntu-debian/master/install-opencv.sh

    bash install-opencv.sh

## Install **tesseract** 

- Install dependencies

        sudo apt-get install autoconf automake libtool
        sudo apt-get install libpng12-dev
        sudo apt-get install libjpeg62-dev
        sudo apt-get install libtiff4-dev
        sudo apt-get install zlib1g-dev
        sudo apt-get install libicu-dev      # (if you plan to make the training tools)
        sudo apt-get install libpango1.0-dev # (if you plan to make the training tools)
        sudo apt-get install libcairo2-dev   # (if you plan to make the training tools)

- Install leptonica.

        cd ~
        wget http://www.leptonica.org/source/leptonica-1.73.tar.gz
        tar xvf leptonica-1.73.tar.gz
        cd leptonica-1.73
        ./configure
        make
        sudo make install

- Fix local library issue.
    
    Open `/etc/ld.so.conf`
        
        sudo nano /etc/ld.so.conf
    
    And add bellow:
        
        include /usr/local/lib
        include /usr
        
    And run this:
        
        sudo ldconfig

- Install tesseract
    
        cd ~
        wget https://codeload.github.com/tesseract-ocr/tesseract/tar.gz/3.04.01
        cd tesseract-3.04.01
        ./autogen.sh
        ./configure
        LDFLAGS="-L/usr/local/lib" CFLAGS="-I/usr/local/include" make
        sudo make install
        sudo ldconfig
        
- Install train data
    
        sudo apt-get install tesseract-ocr
    
        export TESSDATA_PREFIX="/usr/share/tesseract-ocr/"
        
    
## Install **pytesseract**
    
    sudo apt-get install PIL
    sudo apt-get install tesseract-ocr
    
    sudo pip install pytesseract
    
    git clone git@github.com:madmaze/pytesseract.git
    
Place `pytesseract.py` in the same directory of main script.  