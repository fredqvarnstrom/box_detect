#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import argparse
import copy
import os
import sys
import numpy as np
import cv2
import time
import pytesseract
import metadata_utils as meta

try:
    import Image
except ImportError:
    from PIL import Image

base_path = "text_extract/img"

pattern_threshold = 0.2  # threshold which is used for determining color pattern, 0.09 < threshold < 0.3
bound_threshold = 5


def create_dir(dir_path):
    """
    Create Directory if not exists
    :param dir_path:
    :return:
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def paint_with_bg(src_img, axis):
    """
    Remove foreground in the part of image given by axis(x, y, w, h)
    Get most frequency color at the boundary of axis and fill entire area with that color
    :param src_img: source image
    :param axis: area which is referred to get background color
    :return: removed image
    """
    def get_most_frequency_color(dest_img, area):
        """
        Get most frequency color from the given image.
        :param dest_img:
        :param area:
        :return:
        """

        partial_img = dest_img[area[1]:area[1] + area[3], area[0]:area[0] + area[2]]

        max_point = [-1, -1, -1]
        # get max frequent color in the area
        index = 0
        for m in cv2.split(partial_img):
            unique, counts = np.unique(m, return_counts=True)
            max_index = list(counts).index(max(counts))
            max_val = unique[max_index]

            max_point[index] = int(max_val)
            index += 1

        return max_point

    [x1, y1, w1, h1] = axis

    # get most frequency color at the left boundary
    m_left = get_most_frequency_color(src_img, [x1-bound_threshold, y1, bound_threshold, h1])

    m_right = get_most_frequency_color(src_img, [x1+w1, y1, bound_threshold, h1])
    m_top = get_most_frequency_color(src_img, [x1, y1-bound_threshold, w1, bound_threshold])
    m_bottom = get_most_frequency_color(src_img, [x1, y1+h1, w1, bound_threshold])

    tmp_list = [m_left, m_right, m_top, m_bottom]
    m_list = copy.deepcopy(tmp_list)

    for m in tmp_list:
        # remove max color when no boundary
        if m[0] < 0 or m[1] < 0 or m[2] < 0:  # initial value is -1 and it is not changed when there is no boundary
            m_list.remove(m)

    # get color list out of 4 values
    color_r = [tmp[0] for tmp in m_list]
    color_g = [tmp[1] for tmp in m_list]
    color_b = [tmp[2] for tmp in m_list]

    m_total = []
    # get average value
    for col in [color_r, color_g, color_b]:
        m_total.append(reduce(lambda x, y: x + y, col) / len(col))

    # paint destination area with this most frequent color
    for j in range(3):
        src_img[y1:y1 + h1, x1:x1 + w1, j] = int(m_total[j])

    return src_img


def sortkey(a):
    """
    callback of sort function
    :param a: item of list which is needed to sort
    :return: returns the final value - the sorting key is the final value
    """
    return a[-1]


def sortkey_rect_y(a):
    """
    callback of sorting textlines via their y-axis
    :param a:
    :return:
    """
    return a[1]


def sortkey_rect_x(a):
    """
    callback of sorting textlines via their y-axis
    :param a:
    :return:
    """
    return a[0]


def get_inner_freq(img_src, rr):
    """
    Get unique count in the area of image
    If this value is high, this area will be texture, otherwise will have simple color pattern
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    tmp_img = img_src[y:y + h, x:x + w]
    cnt = 0.0
    # get total count of different colors in each channel
    for m in cv2.split(tmp_img):
        unique, counts = np.unique(m, return_counts=True)
        max_cnt = max(counts)
        cnt += max_cnt / w / h

    # print("Color count: ", cnt / 3.0)
    return cnt / 3.0


def get_outer_freg(img_src, rr):
    """
    Get unique count in the boundary of area.
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    thr = bound_threshold
    # get square measure of boundary area
    hei = np.size(img_src, 0)
    wid = np.size(img_src, 1)
    if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
        cnt = 0.0
        # get total count of different colors in each channel
        # top side
        tmp_img = img_src[y - thr:y, x:x + w + thr]
        sq_meas = thr * (w + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # right side
        sq_meas = thr * (h + thr)
        tmp_img = img_src[y:y+h+thr, x+w:x+w+thr]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # bottom side
        sq_meas = thr * (w + thr)
        tmp_img = img_src[y+h:y+h+thr, x-thr:x+w]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # left side
        tmp_img = img_src[y - thr:y + h, x - thr:x]
        sq_meas = thr * (h + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas
        return cnt / 12.0

    else:
        print (rr, "  No boundary")
        return 0


# sorted by y, then X
def arrange_boxes(rr):
    return sorted(rr, key=lambda xx: (xx[1], xx[0]))


def extract_text(file_path, output_folder, mode):
    """
    Extract text lines from the given image and save metadata to csv file.
    :param file_path:  Image file full path
    :param output_folder: path of output files
    :param mode: extraction mode, 1: gradient morph, 0: normal
    :return:
    """
    if output_folder[-1] != "/":
        output_folder += "/"

    create_dir(output_folder)
    create_dir(output_folder + "TextLines")

    img_init = cv2.imread(file_path)    # initial image container
    img_final = cv2.imread(file_path)   # container which has rectangles of each text area
    img_buf = cv2.imread(file_path)     # container where each text area has been in-painted

    try:
        img2gray = cv2.cvtColor(img_init, cv2.COLOR_BGR2GRAY)
    except cv2.error:
        print("Error, cannot find file. Please check file name.")
        return -1

    height = np.size(img_init, 0)
    width = np.size(img_init, 1)

    if mode == 1:
        kernel = np.ones((3, 3), np.uint8)
        gradiented = cv2.morphologyEx(img2gray, cv2.MORPH_GRADIENT, kernel)

        ret, mask = cv2.threshold(gradiented, 180, 255, cv2.THRESH_BINARY)
        image_final = cv2.bitwise_and(gradiented, gradiented, mask=mask)
        ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
    else:
        ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
        image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
        ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text, cv.THRESH_BINARY_INV

    # to manipulate the orientation of dilution,
    # large x means horizonatally dilating more, large y means vertically dilating more
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    dilated = cv2.dilate(new_img, kernel, iterations=6)  # dilate , more the iteration more the dilation

    contoured, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # get contours

    index = 1

    if contours is not None:
        rect_list = [cv2.boundingRect(contour) for contour in contours]
        rect_list.sort(key=sortkey_rect_x)
        rect_list.sort(key=sortkey_rect_y)

        remove_list = []
        features = []
        for rect in rect_list:
            buf = ['TextLine' + str(index), ]
            # get rectangle bounding contour
            [x, y, w, h] = rect

            # Don't plot small false positives that aren't text
            if w < 35 and h < 35:
                # print("Detected textline is too small, ", rect)
                remove_list.append(rect)
                continue

            # remove very large area
            val = 0
            if x < bound_threshold:
                val += 1
            if x + w > width - bound_threshold:
                val += 1
            if y < bound_threshold:
                val += 1
            if y + h > height - bound_threshold:
                val += 1
            if val >= 3:
                remove_list.append(rect)
                print("Detected textline is too large, ", rect)
                continue

            feature = meta.Feature(x, y, w, h)

            # draw rectangle around contour on original image
            cv2.rectangle(img_final, (x, y), (x+w, y+h), (255, 0, 255), 2)

            # save cropped image and extract text
            cropped = img_init[y:y + h, x: x + w]
            s = output_folder + "TextLines/TextLine" + str(index) + "-1.png"
            cv2.imwrite(s, cropped)

            feature.name = "TextLine" + str(index)
            feature.file_path = s

            # extract text from the saved image file
            txt_img = Image.open(s)
            line_text = pytesseract.image_to_string(txt_img)

            if line_text.strip() == '':  # ignore un-detected text area
                os.remove(s)
                remove_list.append(rect)
                continue

            buf += [x, y, w, h, line_text]
            feature.text = line_text

            n = get_inner_freq(img_init, (x, y, w, h))
            if n < pattern_threshold:
                buf.append('Box texture')
                feature.object_texture='Box texture'
            else:
                buf.append('Box plain')
                feature.object_texture='Box plain'

            m = get_outer_freg(img_init, (x, y, w, h))

            if m == 0:
                buf.append('Surround none')
                feature.surround_texture='Surround None'
            elif m < pattern_threshold:
                buf.append('Surround texture')
                feature.surround_texture='Surround Texture'
            else:
                buf.append('Surround plain')
                feature.surround_texture='Surround plain'

            # append number of multi line
            line_number = get_line_number(line_text)
            buf.append(str(line_number))

            # writer_text.writerow(buf)
            features.append(feature)

            print("LINE: ", line_text)

            paint_with_bg(img_buf, [x, y, w, h])

            index += 1

        # save features
        meta.save_to_csv(output_folder + "/metadata_text.csv", features)

        for rm_item in remove_list:
            rect_list.remove(rm_item)

        if len(rect_list) > 0:
            # write original image with added contours to disk
            if b_show_gui:
                cv2.imshow(file_path.split('/')[-1] + ' - Text Detect', img_final)
                cv2.imshow('Inpainted Result', img_buf)
                cv2.waitKey()

            result_folder = output_folder + "/all_objects/Object1"
            create_dir(result_folder)
            cv2.imwrite(result_folder + "/S1-1.png", img_buf)

        return len(rect_list)

    else:
        print("No textline detected with mode ", mode)
        return 0


def get_line_number(line_str):
    """
    Return line number of multi-lined string
    :param line_str:
    :return:
    """
    blank_line = 0
    for line in line_str.splitlines():
        if line.strip() == '':
            blank_line += 1

    return len(line_str.splitlines()) - blank_line


if __name__ == '__main__':

    global b_show_gui

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file_name", required=True, help="Input Image File Name")
    parser.add_argument("-o", "--output_folder_name", required=True, help="Output Folder Name")
    parser.add_argument("-g", "--GUI", help="Set this parameter as 'True'(case sensitive) to see result image")
    parser.add_argument("-showGUI", "--showGUI",
                        help="Set this parameter as 'True'(case sensitive) to see result image")

    inst_arg = parser.parse_args()
    args = vars(inst_arg)

    if args['GUI'] == 'True' or args['showGUI'] == 'True':
        b_show_gui = True
    else:
        b_show_gui = False

    input_file_name = args['input_file_name']
    output_folder_name = args['output_folder_name']

    s_time = time.time()

    # If no textline is detected with normal mode, try again with gradient mode.
    if extract_text(input_file_name, output_folder_name, 0) == 0:
        extract_text(input_file_name, output_folder_name, 1)

    print("Elapsed: ", time.time() - s_time)

    cv2.destroyAllWindows()
