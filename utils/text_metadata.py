from PIL import Image
from tesserocr import PyTessBaseAPI, RIL
import argparse
import os
import wester_feature_extractor_feature as fe
import cv2
import numpy as np
import metadata_utils as meta
import re

# test for textures
pattern_threshold = 0.2  # threshold which is used for determining color pattern, 0.09 < threshold < 0.3
bound_threshold = 5

def populate_text_metadata(input_file_name):
  print "** populate_text_metadata " + input_file_name
  if input_file_name[:1] is not '/':
    input_file_name = os.getcwd() + "/" + input_file_name 

  features = meta.load_from_csv(input_file_name) 
  changed = False
  regex = re.compile('[^a-zA-Z0-9 ]')
  with PyTessBaseAPI() as api:
    for feature in features:
      if (len(feature.text) ==  0 or feature.text == "False") and (feature.file_path is not False):
        print "Looking for text in " + feature.name + " file: " + feature.file_path
        api.SetImageFile(feature.file_path)
        text =  api.GetUTF8Text().strip()
        text = regex.sub('', text)
        confidences = api.AllWordConfidences()
        if (len(confidences) > 0):
          if (confidences[0] > 80):
            print "ACCEPTED TEXT: " + text
            feature.text = text
            changed = True
          else:
            print "REJECTED TEXT: low confidence " + str(confidences[0]) + " " + text

  if changed:
    meta.save_to_csv(input_file_name, features)

def extract_text_from_image_file(input_file_name, output_folder_name):
        pil_image = Image.open(input_file_name)
        image = cv2.imread(input_file_name)
        print "Image " + str(pil_image.size)
        create_dir(output_folder_name)
        file_name = output_folder_name + "/metadata_simple_text.csv"

        features = meta.load_from_csv(file_name) 
        with PyTessBaseAPI() as api:
          api.SetImage(pil_image)
          boxes = api.GetComponentImages(RIL.TEXTLINE, True)
          print 'Found {} textline image components.'.format(len(boxes))
          ti = 1
          valid_textlines = []
          for i, (im, box, _, _) in enumerate(boxes):
              # im is a PIL image object
              # box is a dict with x, y, w and h keys
              api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
              ocrResult = api.GetUTF8Text()
              conf = api.MeanTextConf()
              print (u"Box[{0}]: x={x}, y={y}, w={w}, h={h}, "
                     "confidence: {1}, text: {2}").format(i, conf, ocrResult.encode('ascii', 'ignore'), **box)

              if conf > 60 and box['h'] > 12 and (not ocrResult.isspace()):
                feature = meta.Feature(box['x'], box['y'], box['w'], box['h'])
                feature.text = ocrResult.encode('ascii', 'ignore')
                feature.file_path = input_file_name

                n = get_inner_freq(image, [box['x'], box['y'], box['w'], box['h']])
                if n < pattern_threshold:
                    feature.object_texture="Box texture"
                else:
                    feature.object_texture="Box plain"

                m = get_outer_freg(image, [box['x'], box['y'], box['w'], box['h']])

                if m == 0:
                    feature.surround_texture='Surround none'
                elif m < pattern_threshold:
                    feature.surround_texture='Surround texture'
                else:
                    feature.surround_texture='Surround plain'

                features.append(feature)
                ti = ti + 1


          meta.save_to_csv(file_name , features)

def create_dir(dir_path):
    """
    Create Directory if not exists
    :param dir_path:
    :return:
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

def get_inner_freq(img_src, rr):
    """
    Get unique count in the area of image
    If this value is high, this area will be texture, otherwise will have simple color pattern
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    tmp_img = img_src[y:y + h, x:x + w]
    cnt = 0.0
    # get total count of different colors in each channel
    for m in cv2.split(tmp_img):
        unique, counts = np.unique(m, return_counts=True)
        max_cnt = max(counts)
        cnt += max_cnt / w / h

    #print "Color count: ", cnt / 3.0
    return cnt / 3.0


def get_outer_freg(img_src, rr):
    """
    Get unique count in the boundary of area.
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    thr = bound_threshold
    # get square measure of boundary area
    hei = np.size(img_src, 0)
    wid = np.size(img_src, 1)
    if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
        cnt = 0.0
        # get total count of different colors in each channel
        # top side
        tmp_img = img_src[y - thr:y, x:x + w + thr]
        sq_meas = thr * (w + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # right side
        sq_meas = thr * (h + thr)
        tmp_img = img_src[y:y+h+thr, x+w:x+w+thr]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # bottom side
        sq_meas = thr * (w + thr)
        tmp_img = img_src[y+h:y+h+thr, x-thr:x+w]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # left side
        tmp_img = img_src[y - thr:y + h, x - thr:x]
        sq_meas = thr * (h + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas
        return cnt / 12.0

    else:
        print rr, "  No boundary"
        return 0



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file_name", required=True, help="Input Image File Name")
    #parser.add_argument("-o", "--output_folder_name", required=True, help="Output Folder Name")

    inst_arg = parser.parse_args()
    args = vars(inst_arg)

    input_file_name = args['input_file_name']
    #output_folder_name = args['output_folder_name']

    #extract_text_from_image_file(input_file_name, output_folder_name)
    populate_text_metadata(input_file_name)
